# Free

Developed with Unreal Engine 5

RISKETOS:

- [x] Sistema de inventario base.
- [x] Sistema de interactuables.
- [x] Inteligencia artificial de búsqueda.
- [x] Sistema de inventario ha sido expandido con varios  widgets y puedes dropear o equipar objetos.
- [x] El enemigo ha sido expandido teniendo animaciones acorde a su IA.
- [x] Modificado el sistema de movimiento para utilizar estamina y añadido sistema de vida por HUD.
